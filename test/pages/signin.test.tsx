import React from 'react'
import { render, fireEvent } from '../testUtils'
import SignInPage from '../../src/pages/signin'

describe('Signin page', () => {
  it('matches snapshot', () => {
    const { asFragment } = render(<SignInPage query={{redirect:'/signin'}} />, {})
    expect(asFragment()).toMatchSnapshot()
  })
})
