import {
  VFC,
  ReactNode,
  createContext,
  useState,
  useMemo,
  useCallback,
  useContext,
} from 'react'

import { LoginUser, User } from '../../interfaces'

const _users: (LoginUser & { password: string })[] = [
  {
    username: 'sudhansurana@gmail.com',
    password: 'Test@123',
  },
  {
    username: 'test@user.com',
    password: 'Test@123',
  },
]

const _profiles: User[] = [
  {
    email: 'sudhansurana@gmail.com',
    firstName: 'Sudhansu',
    middleName: 'Sekhar',
    lastName: 'Rana',
    lastLoggedIn: ''
  },
  {
    email: 'test@user.com',
    firstName: 'Test',
    middleName: '',
    lastName: 'User',
    lastLoggedIn: ''
  },
]

export interface AuthContextValue {
  isSignedIn: boolean
  user?: User | undefined
  signIn: (params: {
    username: string
    password: string
  }) => Promise<{ data?: User; error?: Error }>
  signOut: () => void
}

const AuthContext = createContext<AuthContextValue>({
  isSignedIn: false,
  user: undefined,
  signIn: () => Promise.reject(),
  signOut: () => {}, // eslint-disable-line
})

export interface AuthContextProviderProps {
  children: ReactNode
}

export const AuthContextProvider: VFC<AuthContextProviderProps> = ({
  children,
}) => {
  const [user, setUser] = useState<User | undefined>(undefined)

  const isSignedIn = useMemo(() => {
    return !!user
  }, [user])

  const signIn = useMemo<AuthContextValue['signIn']>(() => {
    return async ({ username, password }) => {
      const _loginUser = _users.find(
        (user) => user.username === username && user.password === password
      )

      if (!_loginUser) {
        return { error: new Error() }
      }

      const user: User | undefined = _profiles.find(
        (user) => user.email === username
      )

      setUser(user)

      return { data: user }
    }
  }, [])

  const signOut = useCallback<AuthContextValue['signOut']>(() => {
    setUser(undefined)
  }, [])

  return (
    <AuthContext.Provider
      value={{
        isSignedIn,
        user,
        signIn,
        signOut,
      }}
    >
      {children}
    </AuthContext.Provider>
  )
}

export const useAuthContext: () => AuthContextValue = () => {
  const authContextValue = useContext(AuthContext)

  return authContextValue
}
