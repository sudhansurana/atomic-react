import { VFC } from 'react'

import { Queue } from '../../../interfaces'

import QueueTableRow from '../QueueTableRow'

export interface QueueTableProps {
  queues: Queue[]
}

const QueueTable: VFC<QueueTableProps> = ({ queues }) => {
  return (
    <table className="table-auto w-full">
      <thead>
        <tr className="border-b-2">
          <th className="p-2 text-left">Time</th>
          <th className="p-2 text-left">Severity</th>
          <th className="p-2 text-left w-1/2">Details</th>
          <th className="p-2 text-left">Action</th>
        </tr>
      </thead>
      <tbody>
        {queues.map((queue) => (
          <QueueTableRow key={queue.id} queue={queue} />
        ))}
      </tbody>
    </table>
  )
}

export default QueueTable
