import { VFC, useMemo } from 'react'

import { QueueAction, QueueIndicator, QUEUE_ACTIONS } from '../../../interfaces'

export interface QueueCardIndicatorProps {
    qData: QueueIndicator
}

const QueueCardIndicator: VFC<QueueCardIndicatorProps> = ({ qData }) => {
    return (
        <div className="card bg-light">
            <div className="class-body">
                <h5 className="card-title text-center">{qData.name} Queues</h5>
                <h6 className="card-subtitle mb-2 text-center">({qData.count})</h6>
            </div>
        </div>
    )
}

export default QueueCardIndicator
