import { VFC } from 'react'

import { Queue } from '../../../interfaces'
import QueueCardIndicator from '../QueueCardIndicator'


export interface QueueIndicatorProps {
    indicator: any
}

const QueueIndicator: VFC<QueueIndicatorProps> = ({ indicator }) => {
  console.log('---------',indicator)
  return (
    <div className="row py-4">
      <div className="col-sm-12 col-md-6 col-lg-4">
        <QueueCardIndicator qData={indicator.open}/>
      </div>
      <div className="col-sm-12 col-md-6 col-lg-4">
        <QueueCardIndicator qData={indicator.close}/>
      </div>
      
      <div className="col-sm-12 col-md-6 col-lg-4">
        <QueueCardIndicator qData={indicator.progress}/>
      </div>
    </div>
  )
}

export default QueueIndicator
