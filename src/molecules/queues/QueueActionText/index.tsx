import { VFC, useMemo } from 'react'

import { QueueAction, QUEUE_ACTIONS } from '../../../interfaces'

export interface QueueActionTextProps {
  action: QueueAction
}

const QueueActionText: VFC<QueueActionTextProps> = ({ action }) => {
  const text = useMemo(() => {
    const kv = Object.entries(QUEUE_ACTIONS).find((kv) => kv[1] === action)

    if (!kv) {
      return 'Unknown'
    }

    return kv[0]
  }, [action])

  return <span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800">
    {text}
  </span>
}

export default QueueActionText
