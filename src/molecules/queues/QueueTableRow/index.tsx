import { VFC } from 'react'
import Link from 'next/link'

import { Queue } from '../../../interfaces'

import Anchor from '../../../atoms/Anchor'

import QueueTimeText from '../QueueTimeText'
import QueueActionText from '../QueueActionText'
import QueueSeverityText from '../QueueSeverityText'

export interface QueueTableRowProps {
  queue: Queue
}

const QueueTableRow: VFC<QueueTableRowProps> = ({ queue }) => {
  return (
    <tr className="border-b last:border-b-0 hover:bg-gray-50">
      {/* <td className="p-2">
        <Link href={`/Queues/${queue.id}`} passHref>
          <Anchor>{queue.name}</Anchor>
        </Link>
      </td> */}
      <td className="p-2 text-sm">
        {queue.createdAt && <QueueTimeText createdAt={queue.createdAt} />}
      </td>
      <td className="p-2 text-sm">
        <QueueSeverityText severity={queue.severity} />
      </td>
      <td className="p-2 text-sm">
        {queue.description}
      </td>
      <td className="p-2 text-sm">
        <QueueActionText action={queue.action} />
      </td>
    </tr>
  )
}

export default QueueTableRow
