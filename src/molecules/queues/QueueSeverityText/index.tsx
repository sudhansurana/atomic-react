import { VFC, useMemo } from 'react'

import { QueueSeverity, QUEUE_SEVERITIES } from '../../../interfaces'

export interface QueueSeverityTextProps {
  severity: QueueSeverity
}

const QueueSeverityText: VFC<QueueSeverityTextProps> = ({ severity }) => {
  const text = useMemo(() => {
    const kv = Object.entries(QUEUE_SEVERITIES).find((kv) => kv[1] === severity)

    if (!kv) {
      return 'Unknown'
    }

    return kv[0]
  }, [severity])

  return <>{text}</>
}

export default QueueSeverityText
