import { VFC, useMemo } from 'react'

export interface QueueTimeTextProps {
    createdAt: string
}

const QueueTimeText: VFC<QueueTimeTextProps> = ({ createdAt }) => {
  const text = useMemo(() => {
    console.log('createdAt', createdAt)
    return new Date(createdAt).toLocaleDateString()
  }, [createdAt])

  return <>{text}</>
}

export default QueueTimeText
