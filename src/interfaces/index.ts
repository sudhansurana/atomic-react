export interface User {
  firstName: string;
  middleName?: string;
  lastName: string;
  email: string;
  lastLoggedIn: string;
}

export interface LoginUser {
  username: string;
  password?: string;
}

export interface Queue {
  id: string;
  name: string;
  createdAt: string;
  description?: string;
  severity: QueueSeverity;
  action: QueueAction;
}
export interface QueueIndicator {
  id: string;
  name: string;
  count: number;
}

export const QUEUE_SEVERITIES = {
  High: 'HIGH',
  Low: 'LOW',
  Medium: 'MEDIUM',
} as const



export const QUEUE_ACTIONS = {
  New: 'NEW',
  Assign: 'ASSIGN',
  Canceled: 'CANCELED',
  Progress: 'PROGRESS',
  Done: 'DONE',
} as const

export type QueueSeverity = typeof QUEUE_SEVERITIES[keyof typeof QUEUE_SEVERITIES]


export function isQueueSeverity(value: string): asserts value is QueueSeverity {
  const matched = Object.entries(QUEUE_SEVERITIES).some((kv) => kv[1] === value)
  if (!matched) {
    throw new Error()
  }
}
export type QueueAction = typeof QUEUE_ACTIONS[keyof typeof QUEUE_ACTIONS]

export function isQueueAction(value: string): asserts value is QueueAction {
  const matched = Object.entries(QUEUE_ACTIONS).some((kv) => kv[1] === value)
  if (!matched) {
    throw new Error()
  }
}

