import { VFC, useState, useMemo, useCallback } from 'react'
import { useRouter } from 'next/router'
import { useForm, SubmitHandler } from 'react-hook-form'

import { useAuthContext } from '../../contexts/AuthContext'

import DefaultContainer from '../../components/DefaultContainer'

import TextField from '../../atoms/TextField'
import Label from '../../atoms/Label'
import ButtonPrimary from '../../atoms/Button/Primary'
import ButtonSecondary from '../../atoms/Button/Secondary'

export interface SignInFormValues {
  username?: string
  password?: string
}

export interface SignInTemplateProps {
  redirect?: string
}

const SignInTemplate: VFC<SignInTemplateProps> = ({ redirect }) => {
  const router = useRouter()
  const {
    register,
    handleSubmit,
    reset,
    watch,
  } = useForm<SignInFormValues>()
  const watchedValues = watch()

  const { signIn } = useAuthContext()

  const [isSignInExecuting, setIsSignInExecuting] = useState(false)
  const [isSignInError, setIsSignInError] = useState(false)

  const inputIds = useMemo(() => {
    return {
      username: 'sing_in_username',
      password: 'sign_in_password',
    }
  }, [])

  const isSignInButtonDisabled = useMemo(() => {
    if (
      typeof watchedValues.username === 'undefined' ||
      typeof watchedValues.password === 'undefined'
    ) {
      return true
    }

    if (isSignInExecuting) {
      return true
    }

    if (
      watchedValues.username.length === 0 ||
      watchedValues.password.length === 0
    ) {
      return true
    }

    return false
  }, [watchedValues, isSignInExecuting])

  const onFormSubmit = useCallback<SubmitHandler<SignInFormValues>>(
    ({ username, password }) => {
      ; (async () => {
        if (
          typeof username === 'undefined' ||
          typeof password === 'undefined'
        ) {
          return
        }

        setIsSignInExecuting(true)

        const { error } = await signIn({ username, password })

        if (error) {
          setIsSignInExecuting(false)
          setIsSignInError(true)
          reset({ username, password: '' })
          return
        }

        router.push(redirect || '/')
      })()
    },
    [redirect, reset, signIn] // eslint-disable-line
  )

  const resetData  = () => {
    reset({username: '', password: ''})
  }

  return (
    <main className="w-full h-full py-10 bg-gray-100">
      <DefaultContainer>
        <div className="sm:w-96 m-auto">
          <div className="border rounded-md bg-white p-4 mb-3">
            <div className="text-center mb-3">
              <h1 className="text-xl">Sign In</h1>
            </div>

            {isSignInError && (
              <div className="mb-3">
                <p className="text-red-500">
                  The username or password is incorrect
                </p>
              </div>
            )}

            <form onSubmit={handleSubmit(onFormSubmit)}>
              <div className="mb-5">
                <div className="mb-3">
                  <div className="mb-1">
                    <Label htmlFor={inputIds.username}>username</Label>
                  </div>
                  <div>
                    <TextField
                      type="text"
                      id={inputIds.username}
                      {...register('username', { required: true })}
                      block={true}
                    //error={!!formErrors.username}
                    />
                  </div>
                </div>

                <div className="mb-5">
                  <div className="mb-1">
                    <Label htmlFor={inputIds.password}>Password</Label>
                  </div>
                  <div>
                    <TextField
                      type="password"
                      id={inputIds.password}
                      {...register('password', { required: true })}
                      block={true}
                    //error={!!formErrors.password}
                    />
                  </div>
                </div>
              </div>

              <div className="py-3 sm:flex sm:flex-row">
                <div className="mr-3 w-full">
                  <ButtonPrimary
                    type="submit"
                    disabled={isSignInButtonDisabled}
                    block={true}
                  >
                    Login
                  </ButtonPrimary>
                </div>

                <div className="ml-3 w-full">
                  <ButtonSecondary
                    type="button"
                    disabled={isSignInButtonDisabled}
                    block={true}
                    onClick={resetData}
                  >
                    Reset
                  </ButtonSecondary>
                </div>
              </div>
            </form>
          </div>
        </div>
      </DefaultContainer>
    </main>
  )
}

export default SignInTemplate
