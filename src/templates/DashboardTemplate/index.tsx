import { VFC } from 'react'
import Link from 'next/link'
import DefaultLayout from '../../components/DefaultLayout'
import ContentHeader from '../../molecules/common/ContentHeader'
import AnchorButtonPrimary from '../../atoms/AnchorButtonPrimary'
import ContentMain from '../../molecules/common/ContentMain'
import DefaultContainer from '../../components/DefaultContainer'
import ConnectedQueueTable from '../../organisms/queues/ConnectedQueuesTable'
import ConnectedQueueIndicator from '../../organisms/queues/ConnectedQueueIndicator'

const DashboardTemplate: VFC = () => {
  return (
    <DefaultLayout>
      <main>
        <ContentHeader
          heading="Dashboard"
        />
        <ContentMain>
          <DefaultContainer>
            <ConnectedQueueIndicator/>
            <ConnectedQueueTable/>
          </DefaultContainer>
        </ContentMain>
      </main>
    </DefaultLayout>
  )
}


export default DashboardTemplate