import { VFC, useCallback } from 'react'
import ButtonSecondary from '../../../atoms/Button/Secondary'
import { useIndicatorQueue } from '../../../hooks/api/queues'
import QueueIndicator from '../../../molecules/queues/QueueIndicator'



const ConnectedQueueIndicator: VFC = () => {
  const { data, error, mutate } = useIndicatorQueue()

  const onRetryButtonClick = useCallback(() => {
    mutate()
  }, [mutate])

  if (error) {
    return (
      <div>
        <p className="mb-2">Error occured</p>
        <ButtonSecondary onClick={onRetryButtonClick}>Retry</ButtonSecondary>
      </div>
    )
  }

  if (!data) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    )
  }

  return <QueueIndicator indicator={data} />
}

export default ConnectedQueueIndicator
