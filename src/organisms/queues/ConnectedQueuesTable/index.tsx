import { VFC, useCallback } from 'react'
import ButtonSecondary from '../../../atoms/Button/Secondary'
import { useListQueue } from '../../../hooks/api/queues'
import QueueTable from '../../../molecules/queues/QueuesTable'



const ConnectedQueueTable: VFC = () => {
  const { data, error, mutate } = useListQueue()

  const onRetryButtonClick = useCallback(() => {
    mutate()
  }, [mutate])

  if (error) {
    return (
      <div>
        <p className="mb-2">Error occured</p>
        <ButtonSecondary onClick={onRetryButtonClick}>Retry</ButtonSecondary>
      </div>
    )
  }

  if (!data) {
    return (
      <div>
        <p>Loading...</p>
      </div>
    )
  }

  return <QueueTable queues={data} />
}

export default ConnectedQueueTable
