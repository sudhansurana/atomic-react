import { useState, useMemo } from 'react'
import useSWR, { mutate } from 'swr'
import { v4 as UUID } from 'uuid'

import { Queue, QueueIndicator } from '../../../interfaces'

import { useAuthContext } from '../../../contexts/AuthContext'

// Dummy api methods.
let _queues: Queue[] = [
  {
    id: UUID(),
    name: 'first queue',
    createdAt: '2021-12-21 08:00:01',
    description: 'Lorel ipsum Loren ipsum',
    severity: 'HIGH',
    action: 'ASSIGN',
  },
  {
    id: UUID(),
    name: 'Second queue',
    createdAt: '2021-12-21 09:50:01',
    description: 'Lorel ipsum Loren ipsum',
    severity: 'HIGH',
    action: 'ASSIGN',
  },
  {
    id: UUID(),
    name: 'Third queue',
    createdAt: '2021-12-21 19:45:01',
    description: 'Lorel ipsum Loren ipsum',
    severity: 'MEDIUM',
    action: 'ASSIGN',
  },
]

const _qIndicators = {
  open: {
    id: UUID(),
    name: 'Open',
    count: 23,
  },
  close: {
    id: UUID(),
    name: 'Closed',
    count: 120,

  },
  progress: {
    id: UUID(),
    name: 'In Progress',
    count: 120,

  }
}

const _listQueue: () => Promise<Queue[]> = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(_queues)
    }, 500)
  })
}

const _quesIndicator: () => Promise<any> = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(_qIndicators)
    }, 500)
  })
}

export interface UseListQueueParams {
  isDisabled?: boolean
}

export interface UseListQueueValue {
  data?: Queue[]
  error?: Error
  isValidating: boolean
  mutate: () => void
}

export const useListQueue: (params?: UseListQueueParams) => UseListQueueValue = ({
  isDisabled,
} = {}) => {
  const { isSignedIn } = useAuthContext()

  const { data, error, isValidating, mutate } = useSWR<Queue[], Error>(
    !isDisabled && isSignedIn ? '/api/queues' : null,
    async () => {
      return _listQueue()
    }
  )

  return { data, error, isValidating, mutate } as UseListQueueValue
}

export interface UseIndicatorQueueParams {
  isDisabled?: boolean
}

export interface UseIndicatorQueueValue {
  data?: any
  error?: Error
  isValidating: boolean
  mutate: () => void
}

export const useIndicatorQueue: (params?: UseIndicatorQueueParams) => UseIndicatorQueueValue = ({
  isDisabled,
} = {}) => {
  const { isSignedIn } = useAuthContext()

  const { data, error, isValidating, mutate } = useSWR<Queue[], Error>(
    !isDisabled && isSignedIn ? '/api/queueIndicators' : null,
    async () => {
      return _quesIndicator()
    }
  )

  return { data, error, isValidating, mutate } as UseIndicatorQueueValue
}
