import { NextPage } from 'next'
import PrivateRoot from '../components/PrivateRoot'

import DashboardTemplate from '../templates/DashboardTemplate'

const IndexPage: NextPage = () => {
  return (
    <PrivateRoot>
      <DashboardTemplate />
    </PrivateRoot> 
  )
}

export default IndexPage
